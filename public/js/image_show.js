/* 
    Javascript that will handle the AJAX requests to get the invoice and convert it into an image
*/

var $container = $('.invoice-name');
var $infoModal = $('#myModal');

$container.find('a').on('click', function(e) {
    e.preventDefault();
    var $link = $(e.currentTarget);
    const tokenTag = document.querySelector('#mytoken');
    var $token = tokenTag.dataset.token;

    $.ajax({
        url: '/imageservice',
        data: { client: $link.data('client'), invoice: $link.data('invoice'), token: $token},
        method: 'POST',
        dataType: 'json'
    }).then(function(data) {
        $('#previewImg').attr('src', src='data:image/png;base64,' + data.imageFile);
        $infoModal.find('.modal-title').text(data.client + ' ' + data.invoice);
        $infoModal.modal();
    });
});
