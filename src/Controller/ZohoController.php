<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ZohoDriveConnector;
use App\Form\ZohoFormType;
use Psr\Log\LoggerInterface;

class ZohoController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ZohoDriveConnector $zoho)
    {
        $form = $this->createForm(ZohoFormType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()){
            $url = $zoho->getConsent();
            return $this->redirect($url);
        }

        return $this->render('zoho/index.html.twig', [
            'zohoForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/zoho", name="zoho")
     */
    public function zohoList(Request $request)
    {
        $clients = $this->getFakeList();

        return $this->render('zoho/zoho-list.html.twig', [
            'clients' => $clients,
        ]);
    }

    /**
     * @Route("/imageservice", name="zoho_image")
     */
    public function getImage(Request $request, ZohoDriveConnector $zoho)
    {
        $token = $request->get('token');
        $client = $request->get('client');
        $invoice = $request->get('invoice');
        $zoho->invoicePreview($client, $invoice, $token);

        $imageFile = \file_get_contents('pdf_image.png');
        $imageFile = \base64_encode($imageFile);
        
        return $this->json([
            'client' => $client,
            'invoice' => $invoice,
            'imageFile' => $imageFile
        ]);
    }

    /**
     * @Route("/callback-url", name="callback_url")
     */
    public function callbackUrl(Request $request, ZohoDriveConnector $zoho)
    {
        $code = $request->query->get('code');
        $response = $zoho->createAccessToken($code);

        $clients = $this->getFakeList();
        $accesstoken = $response['access_token'];

        return $this->render('zoho/zoho-list.html.twig',[
            'clients' => $clients,
            'accesstoken' => $accesstoken,
        ]);
    }

    private function getFakeList()
    {
        return [
            [
                'id' => '100801',
                'name' => 'John',
                'invoice_date' => '2020-01-18',
                'invoice' => 'inv_100801_20200104_01',
            ],
            [
                'id' => '100801',
                'name' => 'Bro',
                'invoice_date' => '2020-04-15',
                'invoice' => 'inv_100801_20200104_02',
            ],
            [
                'id' => '100801',
                'name' => 'Yo',
                'invoice_date' => '2019-04-13',
                'invoice' => 'inv_100801_20200104_03',
            ],
        ];
    }

}
