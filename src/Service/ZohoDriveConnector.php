<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ZohoDriveConnector
{
    private $fileHelper;
    private $client;
    private $clientId;
    private $clientSecret;
    private $accessToken;
    private $callbackUrl;
    private $session;
    private $baseUrl;
    private $authorizationHeader;
    private $userId;
    private $acceptType;

    public function __construct(PdfToImageHelper $fileHelper, HttpClientInterface $client)
    {
        //injects the PDF-to-Image service
        $this->fileHelper = $fileHelper;
        $this->client = $client;
        $this->clientId = $_ENV['ZOHO_CLIENT_ID'];
        $this->callbackUrl = $_ENV['ZOHO_CALLBACK_URL'];
        $this->clientSecret = $_ENV['ZOHO_CLIENT_SECRET'];
        $this->baseUrl = 'https://workdrive.zoho.com/api/v1/';
        $this->authorizationHeader = 'Zoho-oauthtoken';
        $this->acceptType = 'application/vnd.api+json';
        $this->userId = $_ENV['ZOHO_STARTING_ID'];
    }

    private function getAuthorizationHeaderValue($accessToken)
    {
        return $this->authorizationHeader . ' ' . $accessToken;
    }

    public function invoicePreview($clientId, $invoiceId, $token)
    {
        $this->accessToken = $token;
        $userId = $this->getCurrentUserId();
        $privateSpaceId = $this->getPrivateSpaceId($userId);
        $folderId = $this->getFolderFromPrivateSpace($privateSpaceId, 'InvoiceArchive');
        $clientFolderId = $this->getClientFolder($folderId, $clientId);
        $fileId = $this->getInvoice($clientFolderId, $invoiceId);
        $this->downloadFile($fileId);
        $this->fileHelper->parsePdf();
    }

    /**
     * 1. Gets the consent to use the drive
     */
    public function getConsent()
    {
        $consentRequest =
            'https://accounts.zoho.com/oauth/v2/auth?response_type=code&client_id=' .
            $this->clientId  .
            '&scope=Workdrive.files.ALL,WorkDrive.workspace.ALL,Workdrive.team.ALL,ZohoSearch.securesearch.READ' .
            '&redirect_uri=' .
            $this->callbackUrl .
            '&prompt=consent';

        return $consentRequest;
    }

    /**
     * 2. Gets the access token to use the drive
     */
    public function createAccessToken($code)
    {
        $response = $this->client->request(
            'POST',
            'https://accounts.zoho.com/oauth/v2/token?client_id=' .
                $this->clientId .
                '&grant_type=authorization_code&' .
                'client_secret=' . $this->clientSecret .
                '&redirect_uri=' . $this->callbackUrl .
                '&code=' . $code
        );

        return $response->toArray();
    }

    /* 
    *  3. ## Get current user of the Team <GET>
    * https://workdrive.zoho.com/api/v1/teams/[ljfr628e]/currentuser
    * data -> id
    */
    private function getCurrentUserId()
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'teams/'.$this->userId .'/currentuser',[
                'headers' => [
                    'Accept' => $this->acceptType,
                    'Authorization' => $this->getAuthorizationHeaderValue($this->accessToken),
                ]
        ]);
        $responseArray = $response->toArray();
        $userId = $responseArray['data']['id'];
        return $userId;
    }

    /** 
     * 4. ## Get user private space <GET>
     * https://workdrive.zoho.com/api/v1/users/[ljfr628e-881892]/privatespace
     * data -> id
    */
    private function getPrivateSpaceId($userId)
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'users/'.$userId .'/privatespace',[
                'headers' => [
                    'Accept' => $this->acceptType,
                    'Authorization' => $this->getAuthorizationHeaderValue($this->accessToken),
                ]
        ]);
        $privateSpaceArray = $response->toArray();
        $privateSpaceId = $privateSpaceArray['data'][0]['id'];
        return $privateSpaceId;
    }

    /** 
     * 5. ## User Folder from Private Space
     * https://workdrive.zoho.com/api/v1/privatespace/[ljfr63d98]/files
     * data -> attributes -> "display_attr_name": "InvoiceArchive",
     * 
    */
    private function getFolderFromPrivateSpace($privateSpaceId, $folderName)
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'privatespace/'.$privateSpaceId .'/files',[
                'headers' => [
                    'Accept' => $this->acceptType,
                    'Authorization' => $this->getAuthorizationHeaderValue($this->accessToken),
                ]
        ]);
        $responseArray = $response->toArray();
        $folderId = '';
        foreach ($responseArray['data'] as $item)
        {
            if ($item['attributes']['name']== $folderName)
            {
                $folderId = $item['id'];
            }
        }
        return $folderId;
    }

    /** 
    * 6. ## User Folders from Folder 
    * https://workdrive.zoho.com/api/v1/files/[fi393bb969]/files
    * data -> attributes ->  "display_attr_name": "100801",
    */
    private function getClientFolder($folderId, $clientId)
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'files/'.$folderId.'/files',[
                'headers' => [
                    'Accept' => $this->acceptType,
                    'Authorization' => $this->getAuthorizationHeaderValue($this->accessToken),
                ]
        ]);
        $responseArray = $response->toArray();
        $folderId = '';
        foreach ($responseArray['data'] as $item)
        {
            if ($item['attributes']['name']== $clientId)
            {
                $folderId = $item['id'];
            }
        }
        return $folderId;
    }

    /** 
     * 7. ## Use files from Folder 800101
     * https://workdrive.zoho.com/api/v1/files/[fi393804b9]/files
     * data -> attributes ->  "display_attr_name": "inv_100801_20200104_03.pdf"
    */
    public function getInvoice($folderId, $invoiceId)
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'files/'.$folderId.'/files',[
                'headers' => [
                    'Accept' => $this->acceptType,
                    'Authorization' => $this->getAuthorizationHeaderValue($this->accessToken),
                ]
        ]);

        $responseArray = $response->toArray();
        $fileId = '';
        foreach ($responseArray['data'] as $item)
        {
            if ($item['attributes']['display_attr_name']== ($invoiceId.'.pdf'))
            {
                $fileId = $item['id'];
            }
        }
        return $fileId;
    }

    /*
        8. ## Download the correct file
        https://workdrive.zoho.com/api/v1/download/[fi393ac]
    */
    public function downloadFile($fileId)
    {
        $response = $this->client->request(
            'GET',
            $this->baseUrl.'download/'.$fileId ,[
                'headers' => [
                    'Accept' => '*/*',
                    'Authorization' => $this->getAuthorizationHeaderValue($this->accessToken),
                ]
        ]);

        $handler = fopen('temp.pdf','w');
        foreach ($this->client->stream($response) as $chunk) {
            fwrite($handler, $chunk->getContent());
        }
        fclose($handler);
    }
}
