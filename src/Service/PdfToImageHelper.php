<?php 

namespace App\Service;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Spatie\PdfToImage\Pdf;

class PdfToImageHelper
{
    public function parsePdf()
    {
        try { 
            $stream = \file_get_contents('temp.pdf');
            $pdf = new Pdf('temp.pdf');
            $outputImagePath = 'pdf_image.png';
            $pdf->setOutputFormat('png');
            $pdf->saveImage($outputImagePath);
        } catch (Exception $e) {
            dump($e);
        }
    }
}