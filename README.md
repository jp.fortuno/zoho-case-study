
1. ## Consent <GET>
https://accounts.zoho.com/oauth/v2/auth?response_type=code&client_id=[client_id]&scope=Workdrive.files.ALL,WorkDrive.workspace.ALL,Workdrive.team.ALL,ZohoSearch.securesearch.READ&redirect_uri=https://zohoworkdrive.herokuapp.com/&prompt=consent

2. ## Auth <POST>

https://accounts.zoho.com/oauth/v2/token?client_id=[client_id]&grant_type=authorization_code&client_secret=[client_secret]&redirect_uri=https://zohoworkdrive.herokuapp.com/&code=[code_returned]

- All requests must have the following Header keys/values:
Authorization: Zoho-oauthtoken [auth_token]
Accept: application/vnd.api+json

3. ## Get current user of the Team <GET>
https://workdrive.zoho.com/api/v1/teams/[ljfr628e]/currentuser
data -> id


4. ## Get user private space <GET>

https://workdrive.zoho.com/api/v1/users/[ljfr628e-881892]/privatespace
data -> id

5. ## User Folder from Private Space

https://workdrive.zoho.com/api/v1/privatespace/[ljfr63d98]/files
data -> attributes -> "display_attr_name": "InvoiceArchive",

6. ## User Folders from Folder 
https://workdrive.zoho.com/api/v1/files/[fi393bb969]/files
data -> attributes ->  "display_attr_name": "100803",

7. ## Use files from Folder 800101

https://workdrive.zoho.com/api/v1/files/[fi393804b9]/files
data -> attributes ->  "display_attr_name": "inv_100801_20200104_03.pdf"

8. ## Download the correct file
https://workdrive.zoho.com/api/v1/download/[fi393ac]

